package com.rahbee.stock.utilities;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import com.rahbee.stock.entity.Stock;

public class JpaUtils {

	public static void persistStock(EntityManagerFactory factory, EntityManager entityManager, Stock stockParam) {
		EntityTransaction transaction = entityManager.getTransaction();
		try {
			transaction.begin();
			Stock stock = new Stock();
			stock.setStockCode(stockParam.getStockCode());
			stock.setStockCount(stockParam.getStockCount());
			stock.setStockPrice(stockParam.getStockPrice());
			entityManager.persist(stock);
			transaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
			if (transaction.isActive()) {
				transaction.rollback();
			}
		}
	}
}
