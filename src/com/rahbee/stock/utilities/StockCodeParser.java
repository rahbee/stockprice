package com.rahbee.stock.utilities;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.text.DecimalFormat;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.stream.JsonParser;
import javax.json.stream.JsonParser.Event;

public class StockCodeParser {

	private static String getPrice(String stockCode) throws IOException {
		String price = "0.0";
		URL url = new URL("http://finance.yahoo.com/webservice/v1/symbols/"+ stockCode.trim() +"/quote?format=json");
		try (InputStream is = url.openStream(); JsonReader rdr = Json.createReader(is)) {

			JsonObject obj = rdr.readObject();
			JsonObject listObj = obj.getJsonObject("list");
			if (listObj.getJsonObject("meta").getInt("count") > 0) {
				JsonArray results = listObj.getJsonArray("resources");
				// Assume only one array item
				for (JsonObject result : results.getValuesAs(JsonObject.class)) {
					price = result.getJsonObject("resource").getJsonObject("fields").getString("price");
				}
			} else {
				// stockFound = false;
			}

		}
		
		return price;
	}

	public static Double getResult(String stockCode, int count) {
		Double result = 0.0;
		try {
			if (stockCode.trim().length() > 0 && count > 0){
				result = Double.parseDouble(getPrice(stockCode)) * count;
			}
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}

}
