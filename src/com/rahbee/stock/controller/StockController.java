package com.rahbee.stock.controller;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.rahbee.stock.entity.Stock;
import com.rahbee.stock.utilities.JpaUtils;
import com.rahbee.stock.utilities.StockCodeParser;

@Controller
public class StockController {

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public ModelAndView stock() {
		return new ModelAndView("stock", "command", new Stock());
	}

	@RequestMapping(value = "/calculate", method = RequestMethod.POST)
	public String calculate(@ModelAttribute("SpringWeb") Stock stock, ModelMap model) {
		Double result = StockCodeParser.getResult(stock.getStockCode(), stock.getStockCount());

		if (result != 0.0) {
			model.addAttribute("totalPrice", result);
			System.out.println("Result" + result);
			stock.setStockPrice(result);
			// Persist data
			EntityManagerFactory factory = null;
			EntityManager entityManager = null;
			try {
				factory = Persistence.createEntityManagerFactory("test");
				entityManager = factory.createEntityManager();

				JpaUtils.persistStock(factory, entityManager, stock);

			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				if (entityManager != null) {
					entityManager.close();
				}
				if (factory != null) {
					factory.close();
				}
			}

		} else {
			model.addAttribute("totalPrice", "Calculation error!");
		}
		return "result";
	}

}
