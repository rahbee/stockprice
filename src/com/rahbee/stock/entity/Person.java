package com.rahbee.stock.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.TableGenerator;

@Entity
public class Person {
	
	@Id
	@TableGenerator
	(name="TABLE_GENERATOR" , table= "ID_TABLE", pkColumnName= "ID_TABLE_NAME", pkColumnValue= "PERSON_ID", valueColumnName= "ID_TABLE_VALUE")
	@GeneratedValue
	(strategy = GenerationType.AUTO)
	
	private int Id;
	private String stockCode;
	private int stockCount;
	
	public String getStockCode() {
		return stockCode;
	}
	public void setStockCode(String stockCode) {
		this.stockCode = stockCode;
	}
	public int getStockCount() {
		return stockCount;
	}
	public void setStockCount(int count) {
		this.stockCount = count;
	}
}
