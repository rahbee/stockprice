package com.rahbee.stock.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

@Entity
@Table(name = "stock")
public class Stock {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long Id;
	
	@Column(name="STOCK_CODE", nullable=false, length=50)
	private String stockCode;
	
	@Column(name="STOCK_COUNT", nullable=false)
	private int stockCount;
	
	@Column(name="STOCK_PRICE", nullable=true, columnDefinition="Decimal(10,6) default '0.000000'")
	private double stockPrice = 0.0;

	public String getStockCode() {
		return stockCode;
	}

	public void setStockCode(String stockCode) {
		this.stockCode = stockCode;
	}

	public int getStockCount() {
		return stockCount;
	}

	public void setStockCount(int count) {
		this.stockCount = count;
	}
	
	public double getStockPrice() {
		return stockPrice;
	}

	public void setStockPrice(double stockPrice) {
		this.stockPrice = stockPrice;
	}
}
